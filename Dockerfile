FROM ubuntu
LABEL "project"="char"
LABEL "author"="hoysala"
RUN apt update
RUN apt install apache2 -y
CMD ["/usr/sbin/apache2ctl","-D","FOREGROUND"]
Expose 80
WORKDIR /var/www/html/
VOLUME /var/log/apache2/
ADD character.tar.gz /var/www/html/